package update.mercurial;

import models.Contribution;
import models.ContributionEvent;
import models.ContributionEventType;
import play.Logger;
import update.BetterrevActor;
import utils.Processes;

import java.io.IOException;

import static models.ContributionEventType.CONTRIBUTION_GENERATED;
import static models.ContributionEventType.CONTRIBUTION_MODIFIED;
import static models.ContributionEventType.WEBREV_GENERATED;

/**
 * Generates webrevs based upon a Contribution diffed against the current openjdk repository
 */
public class WebrevGenerator extends BetterrevActor {

    private static final String WEBREV_SCRIPT = "adopt/generate_webrev.sh";

    private String adoptDirectory;

    public WebrevGenerator() {
        this.adoptDirectory = "adopt";
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof ContributionEvent)) {
            unhandled(message);
            return;
        }

        ContributionEvent event = (ContributionEvent) message;
        ContributionEventType type = event.contributionEventType;
        if (type != CONTRIBUTION_GENERATED && type != CONTRIBUTION_MODIFIED) {
            return;
        }

        Logger.debug("ContributionEvent of type " + type + " received.");

        int exitCode = generateWebrev(event);

        if (exitCode != 0) {
//            TODO: proper error handling around this process
//            Also bear in mind exceptions
        }

        ContributionEvent webRevGenerated = new ContributionEvent(WEBREV_GENERATED);
        eventStream().publish(webRevGenerated);
    }

    private int generateWebrev(ContributionEvent event) throws IOException {
        Contribution contribution = event.contribution;
        String repository = contribution.openJdkRepoName();
        String remote = contribution.requestersRepositoryUrl();
        String resultLocation = contribution.webrevLocation().getPath();

        int exitCode = Processes.runThroughShell(adoptDirectory, WEBREV_SCRIPT, repository, remote, resultLocation);
        return exitCode;
    }

}
