package update.pullrequest;

import models.Contribution;

import java.util.List;

/**
 * @author Edward Yue Shung Wong
 */
public final class PullRequestsImportedEvent {
    private List<Contribution> contributions;

    public PullRequestsImportedEvent(List<Contribution> contributions) {
        this.contributions = contributions;
    }

    public List<Contribution> getContributions() {
        return contributions;
    }
}
